package android.example.fisiapp.preguntas;

import java.io.Serializable;

public class PreguntasPojo implements Serializable {

    private String pregunta;
    private String respuesta;

    public PreguntasPojo(String pregunta, String respuesta) {
        this.pregunta = pregunta;
        this.respuesta = respuesta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
