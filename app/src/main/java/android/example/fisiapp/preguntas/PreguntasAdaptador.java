package android.example.fisiapp.preguntas;

import android.content.Context;
import android.example.fisiapp.R;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PreguntasAdaptador extends RecyclerView.Adapter<PreguntasAdaptador.NumberViewHolder> {

    private static final String TAG = PreguntasAdaptador.class.getSimpleName();
    private int mNumberItems;
    public ArrayList<PreguntasPojo> arreglo = new ArrayList<>();

    public PreguntasAdaptador(int numberOfItems) {
        mNumberItems = numberOfItems;
    }

    @Override
    public PreguntasAdaptador.NumberViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType){
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.recycler_preguntas;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        PreguntasAdaptador.NumberViewHolder viewHolder = new PreguntasAdaptador.NumberViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PreguntasAdaptador.NumberViewHolder holder, int position) {
        Log.d(TAG, "#" + position);
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    public void setArreglo(Bundle parreglo){
        for (String key : parreglo.keySet()) {
            PreguntasPojo temp = (PreguntasPojo) parreglo.get(key);
            arreglo.add(temp);
        }
        notifyDataSetChanged();
    }

    class NumberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView pregunta;
        TextView mensaje;
        ImageView flecha;

        public NumberViewHolder(View itemView){
            super(itemView);

            pregunta = (TextView) itemView.findViewById(R.id.pregunta);
            mensaje = (TextView) itemView.findViewById(R.id.mensaje);
            flecha = itemView.findViewById(R.id.flecha);
            itemView.setOnClickListener(this);
        }

        void bind(int position)
        {
            try
            {
                final PreguntasPojo temp = arreglo.get(position);
                pregunta.setText(temp.getPregunta());
                mensaje.setText(temp.getRespuesta());

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            if(mensaje.getVisibility() == View.GONE)
            {
                mensaje.setVisibility(View.VISIBLE);
                flecha.setImageResource(R.drawable.ic_flecha_abajo);
            }
            else
            {
                mensaje.setVisibility(View.GONE);
                flecha.setImageResource(R.drawable.ic_flecha);
            }
        }
    }
}
