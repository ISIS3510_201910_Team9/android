package android.example.fisiapp.preguntas;

import android.content.Context;
import android.example.fisiapp.R;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class PreguntasFragment extends Fragment {

    public ArrayList <PreguntasPojo> lista = new ArrayList<>();

    private PreguntasAdaptador mAdapter;
    private RecyclerView mNumberList;

    public PreguntasFragment() {
        // Required empty public constructor
    }

    public static PreguntasFragment newInstance() {
        PreguntasFragment res = new PreguntasFragment();
        return res;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_preguntas, container, false);

        PreguntasPojo pojo1 = new PreguntasPojo("¿Cómo sé la forma de hacer un ejercicio?", "El fisioterapeuta te va a proveer un video para que veas cómo realizar tus ejercicios.");
        PreguntasPojo pojo2 = new PreguntasPojo("¿Quién actualiza mis ejercicios?", "El fisioterapeuta encargado va a agregarte ejercicios desde su plataforma.");
        PreguntasPojo pojo3 = new PreguntasPojo("¿Cómo me contacto con un fisiterapeuta?", "Al abrir el mapa, puedes seleccionar un sitio para que se abra su información, una vez se abra, al hacer click en el mensaje, te vamos a redirigir a su perfil en WhatsApp.");
        PreguntasPojo pojo4 = new PreguntasPojo("¿Puedo usar la aplicación sin conexión a internet?", "Para ingresar en la aplicación debes tener conexión a internet. Una vez dentro, puedes validar ejercicios sin conexión a internet y actualizaremos la información cuando tengas internet. Puedes usar el mapa aunque no verás los fitioterapeutas.");
        PreguntasPojo pojo5 = new PreguntasPojo("¿Puedo repetir un ejercicio ya hecho?", "¡Claro que sí! Puedes repetir todos los ejercicios que desees cuando quieras.");
        PreguntasPojo pojo6 = new PreguntasPojo("¿Qué formas hay para registrarme en la aplicación?", "Puedes crear una cuenta con tu correo electrónico para que te envíemos un correo de verificación. También puedes acceder mediante tu cuenta de Google.");
        lista.add(pojo1);
        lista.add(pojo2);
        lista.add(pojo3);
        lista.add(pojo4);
        lista.add(pojo5);
        lista.add(pojo6);

        final Bundle arreglo = new Bundle();

        for (int pos = 0;pos < lista.size(); pos++)
        {
            arreglo.putSerializable("preguntas" + pos, lista.get(pos));
        }

        mNumberList = (RecyclerView) view.findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mNumberList.setLayoutManager(layoutManager);
        mNumberList.setHasFixedSize(true);
        mAdapter = darAdaptador(arreglo);
        mAdapter.setArreglo(arreglo);
        mNumberList.setAdapter(mAdapter);

        final ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (!wifi.isConnectedOrConnecting() && !mobile.isConnectedOrConnecting()) {

            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "No tienes internet", Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
            snackbar.show();
        }
        return view;
    }

    public PreguntasAdaptador darAdaptador(Bundle bundle) {

        PreguntasAdaptador temp = new PreguntasAdaptador(bundle.size());
        return temp;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
