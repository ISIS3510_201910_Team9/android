package android.example.fisiapp.historial;

import android.example.fisiapp.persistencia.InfoEjercicioPojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class HistorialPojo implements Serializable, Comparable<HistorialPojo> {
    private String fecha;
    private boolean completo = true;
    private ArrayList<InfoEjercicioPojo> ejercicios = new ArrayList<>();

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public boolean isCompleto() {
        return completo;
    }

    public void establecerCompleto() {

        for (InfoEjercicioPojo info: ejercicios) {
            if(!info.isCompletitud())
            {
                completo = false;
                return;
            }
        }
    }

    public ArrayList<InfoEjercicioPojo> getEjercicios() {
        return ejercicios;
    }

    public void setEjercicios(ArrayList<InfoEjercicioPojo> ejercicios) {
        this.ejercicios = ejercicios;
    }

    public void addEjercicio(InfoEjercicioPojo ejercicio) {
        this.ejercicios.add(ejercicio);
    }

    @Override
    public int compareTo(HistorialPojo o) {
        String[] o1Fecha = this.getFecha().split("-");
        String[] o2Fecha = o.getFecha().split("-");
        if(Integer.parseInt(o1Fecha[2]) < Integer.parseInt(o2Fecha[2]))
        {
            return 1;
        }
        else if(Integer.parseInt(o1Fecha[2]) > Integer.parseInt(o2Fecha[2]))
        {
            return -1;
        }
        else
        {
            if(Integer.parseInt(o1Fecha[1]) < Integer.parseInt(o2Fecha[1]))
            {
                return 1;
            }
            else if(Integer.parseInt(o1Fecha[1]) > Integer.parseInt(o2Fecha[1]))
            {
                return -1;
            }
            else
            {
                if(Integer.parseInt(o1Fecha[0]) < Integer.parseInt(o2Fecha[0]))
                {
                    return 1;
                }
                else if(Integer.parseInt(o1Fecha[0]) > Integer.parseInt(o2Fecha[0]))
                {
                    return -1;
                }
            }
        }
        return 0;
    }
}
