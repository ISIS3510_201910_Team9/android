package android.example.fisiapp.historial;

import android.example.fisiapp.R;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class HistorialAdaptador extends RecyclerView.Adapter<HistorialAdaptador.NumberViewHolder>  {

    private static final String TAG = HistorialAdaptador.class.getSimpleName();
    private final ListItemClickListener mOnClickListener;
    private int mNumberItems;
    public ArrayList<HistorialPojo> arreglo = new ArrayList<>();


    public interface ListItemClickListener{
        void onListItemClick(int clickedItemIndex, HistorialPojo historialPojo);
    }

    public HistorialAdaptador(int numberOfItems, ListItemClickListener listener) {
        mNumberItems = numberOfItems;
        mOnClickListener = listener;
    }

    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType){
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.recycler_historial;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        NumberViewHolder viewHolder = new NumberViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        Log.d(TAG, "#" + position);
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    public void setArreglo(Bundle parreglo){
        for (String key : parreglo.keySet()) {
            HistorialPojo temp = (HistorialPojo) parreglo.get(key);
            arreglo.add(temp);
        }
        notifyDataSetChanged();
    }

    class NumberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView fecha;
        TextView texto1;
        TextView texto2;
        ImageView imagen;
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMMM");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd");

        public NumberViewHolder(View itemView){
            super(itemView);

            fecha = (TextView) itemView.findViewById(R.id.fecha);
            texto1 = (TextView) itemView.findViewById(R.id.texto1);
            texto2 = (TextView) itemView.findViewById(R.id.texto2);
            imagen = (ImageView) itemView.findViewById(R.id.imagen);
            itemView.setOnClickListener(this);
        }

        void bind(int position)
        {
            try
            {
                HistorialPojo temp = arreglo.get(position);
                if(temp.getEjercicios().size()>0)
                {
                    fecha.setText(sdf2.format(temp.getEjercicios().get(0).getCreacion()) + " " + sdf1.format(temp.getEjercicios().get(0).getCreacion()));
                }

                double totales = temp.getEjercicios().size();
                double completos = 0;

                for(int pos = 0; pos < totales; pos++)
                {
                    if(temp.getEjercicios().get(pos).isCompletitud())
                    {
                        completos++;
                    }
                }
                double division = completos/totales;
                texto1.setText("" + (int) completos);
                texto2.setText("" + (int) totales);

                Log.d("completos", "" + completos);
                Log.d("totales", "" + totales);
                Log.d("divi", "" + division);

                if(division == 0)
                {
                    imagen.setImageResource(R.drawable.ic_circulo_0);
                }
                else if(completos == totales)
                {
                    imagen.setImageResource(R.drawable.ic_circulo_6);
                }
                else if(division < 0.25)
                {
                    imagen.setImageResource(R.drawable.ic_circulo_1);
                }
                else if(division < 0.5)
                {
                    imagen.setImageResource(R.drawable.ic_circulo_2);
                }
                else if(division < 0.75)
                {
                    imagen.setImageResource(R.drawable.ic_circulo_3);
                }
                else
                {
                    imagen.setImageResource(R.drawable.ic_circulo_4);
                }

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition, arreglo.get(clickedPosition));

        }
    }
}