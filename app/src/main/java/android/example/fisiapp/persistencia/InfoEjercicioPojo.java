package android.example.fisiapp.persistencia;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.example.fisiapp.historial.HistorialPojo;

import java.io.Serializable;
import java.util.Date;


@Entity(tableName = "InfoEjercicio")
public class InfoEjercicioPojo implements Serializable, Comparable<InfoEjercicioPojo> {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String nombre;
    private String descripcion;
    private String tipo;
    private Long repeticiones;
    private Date creacion;
    private boolean completitud;
    private Long duracion;
    private Long cantidadRepeticiones;
    private String video;
    private String idFireBase;

    @Ignore
    public InfoEjercicioPojo(String nombre, String descripcion, String tipo, Long repeticiones, Date creacion, boolean completitud, Long duracion, Long cantidadRepeticiones, String video, String idFireBase) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.repeticiones = repeticiones;
        this.creacion = creacion;
        this.completitud = completitud;
        this.duracion = duracion;
        this.cantidadRepeticiones = cantidadRepeticiones;
        this.video = video;
        this.idFireBase = idFireBase;
    }

    public InfoEjercicioPojo(int id, String nombre, String descripcion, String tipo, Long repeticiones, Date creacion, boolean completitud, Long duracion, Long cantidadRepeticiones, String video, String idFireBase) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.repeticiones = repeticiones;
        this.creacion = creacion;
        this.completitud = completitud;
        this.duracion = duracion;
        this.cantidadRepeticiones = cantidadRepeticiones;
        this.video = video;
        this.idFireBase = idFireBase;
    }

    public String getIdFireBase() {
        return idFireBase;
    }

    public void setIdFireBase(String idFireBase) {
        this.idFireBase = idFireBase;
    }

    public Long getCantidadRepeticiones() {
        return cantidadRepeticiones;
    }

    public void setCantidadRepeticiones(Long cantidadRepeticiones) {
        this.cantidadRepeticiones = cantidadRepeticiones;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Long getDuracion() {
        return duracion;
    }

    public void setDuracion(Long duracion) {
        this.duracion = duracion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getRepeticiones() {
        return repeticiones;
    }

    public void setRepeticiones(Long repeticiones) {
        this.repeticiones = repeticiones;
    }

    public Date getCreacion() {
        return creacion;
    }

    public void setCreacion(Date creacion) {
        this.creacion = creacion;
    }

    public boolean isCompletitud() {
        return completitud;
    }

    public void setCompletitud(boolean completitud) {
        this.completitud = completitud;
    }

    @Override
    public int compareTo(InfoEjercicioPojo o) {
        return this.creacion.compareTo(o.getCreacion());
    }
}
