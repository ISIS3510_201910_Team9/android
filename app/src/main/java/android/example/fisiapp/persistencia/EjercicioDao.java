package android.example.fisiapp.persistencia;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


@Dao
public interface EjercicioDao {
    @Query("SELECT * FROM InfoEjercicio ORDER BY creacion")
    LiveData<List<InfoEjercicioPojo>> loadAllEjercicios();

    @Insert
    void insertEjercicio(InfoEjercicioPojo infoEjer);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateEjercicio(InfoEjercicioPojo infoEjer);

    @Delete
    void deleteEjercicio(InfoEjercicioPojo infoEjer);

    @Query("DELETE FROM InfoEjercicio")
    void nukeTable();
}
