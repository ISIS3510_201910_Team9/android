package android.example.fisiapp.persistencia;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.util.Log;


@Database(entities = {InfoEjercicioPojo.class}, version = 1, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class EjercicioDatabase extends RoomDatabase {

    private static final String LOG_TAG = EjercicioDatabase.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "todolist";
    private static EjercicioDatabase sInstance;

    public static EjercicioDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                Log.d(LOG_TAG, "Creating new database instance");
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        EjercicioDatabase.class, EjercicioDatabase.DATABASE_NAME)
                        // TODO (2) call allowMainThreadQueries before building the instance
                        .build();
            }
        }
        Log.d(LOG_TAG, "Getting the database instance");
        return sInstance;
    }

    public abstract EjercicioDao ejercicioDao();

}
