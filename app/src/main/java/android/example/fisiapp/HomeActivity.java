package android.example.fisiapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.example.fisiapp.videoejercicio.Tokens;
import android.example.fisiapp.detailhistorial.DetailHistorialFragment;
import android.example.fisiapp.historial.HistorialFragment;
import android.example.fisiapp.listadiaria.HomeFragment;
import android.example.fisiapp.login.CalidadPeticion;
import android.example.fisiapp.login.IngresoActivity;
import android.example.fisiapp.mapa.MapsActivityCurrentPlace;
import android.example.fisiapp.persistencia.AppExecutors;
import android.example.fisiapp.persistencia.EjercicioDatabase;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.example.fisiapp.preguntas.PreguntasFragment;
import android.example.fisiapp.videoejercicio.Tokens;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements DetailHistorialFragment.OnFragmentInteractionListener, HomeFragment.OnFragmentInteractionListener , HistorialFragment.OnFragmentInteractionListener{

    public static String NOMBRE_BUNDLEAC = "NOMBRE_BUNDLE";
    public static Bundle INFO_BUNDLEAC;
    public EjercicioDatabase mDb;
    public boolean finalizoFirebase;

    private FragmentTransaction transition;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if(!finalizoFirebase)
                return true;
            switch (item.getItemId()) {
                case R.id.navigation_ejercicios:
                    WeakReference<HomeFragment> homeFragment = new WeakReference<> (new HomeFragment());
                    transition = getSupportFragmentManager().beginTransaction();
                    transition.replace(R.id.contenedor,homeFragment.get()).commit();
                    return true;
                case R.id.navigation_historial:
                    WeakReference<HistorialFragment> historialFragment = new WeakReference<> (new HistorialFragment());
                    transition = getSupportFragmentManager().beginTransaction();
                    transition.replace(R.id.contenedor,historialFragment.get()).commit();
                    return true;
                case R.id.navigation_notifications:
                    WeakReference<PreguntasFragment> preguntasFragment = new WeakReference<> (new PreguntasFragment());
                    transition = getSupportFragmentManager().beginTransaction();
                    transition.replace(R.id.contenedor,preguntasFragment.get()).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case CalidadPeticion.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CalidadPeticion.darCalidad(this);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        finalizoFirebase = false;

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //FIREBASE
        mDb = EjercicioDatabase.getInstance(getApplicationContext());
        final ArrayList<InfoEjercicioPojo> listaarreglo = new ArrayList<>();
        final DocumentReference mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1");

        final ConnectivityManager connMgr = (ConnectivityManager)
                this.getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting() || mobile.isConnectedOrConnecting()) {
            CalidadPeticion.darCalidad(this);
            mDocRef.collection("Ejercicios")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                               @Override
                                               public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                                   if (task.isSuccessful()) {
                                                       for (QueryDocumentSnapshot document : task.getResult()) {
                                                           String nombre = document.getString("nombre");
                                                           String descripcion = document.getString("descripcion");
                                                           String tipo = document.getString("tipo");
                                                           Long repeticiones = (Long) document.get("repeticiones");
                                                           Long creacion = (Long) document.get("creacion");
                                                           Long duracion = (Long) document.get("duracion");
                                                           Long cantidadRepeticiones = (Long) document.get("cantidadRepeticiones");
                                                           String idFireBase = document.getId();
                                                           String video = document.getString("video");
                                                           Boolean completitud = (Boolean) document.getBoolean("completitud");

                                                           InfoEjercicioPojo pojoTemp = new InfoEjercicioPojo(nombre, descripcion, tipo, repeticiones, new Date(creacion), completitud, duracion, cantidadRepeticiones, video, idFireBase);
                                                           listaarreglo.add(pojoTemp);
                                                       }
                                                       checkearInsertar(listaarreglo);
                                                       WeakReference<HomeFragment> homeFragment = new WeakReference<> (new HomeFragment());
                                                       getSupportFragmentManager().beginTransaction().add(R.id.contenedor, homeFragment.get()).commit();
                                                       finalizoFirebase = true;

                                                   } else {
                                                       Log.d("prueba1", "Error getting documents: ", task.getException());
                                                   }
                                               }
                                           }
                    );
        }
        else{
            WeakReference<HomeFragment> homeFragment = new WeakReference<> (new HomeFragment());
            getSupportFragmentManager().beginTransaction().add(R.id.contenedor, homeFragment.get()).commit();
            finalizoFirebase = true;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void checkearInsertar(final List<InfoEjercicioPojo> plist){
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.ejercicioDao().nukeTable();
                for (InfoEjercicioPojo temp: plist) {
                    mDb.ejercicioDao().insertEjercicio(temp);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.actionstoolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if(!finalizoFirebase)
            return true;
        switch (item.getItemId()) {
            case R.id.sign_out:
                sign_out();
                return true;
            case R.id.mapa:
                irAlMapa();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void irAlMapa()
    {
        if(!finalizoFirebase)
        {
            return;
        }
        Intent intentVe = new Intent(this, MapsActivityCurrentPlace.class);
        if(getIntent().getStringExtra("google") != null)
        {
            intentVe.putExtra("google", "google");
        }
        else {
            intentVe.putExtra("correo", "correo");
        }
        startActivity(intentVe);
    }

    private void sign_out()
    {
        if(!finalizoFirebase)
        {
            return;
        }

        if(getIntent().getStringExtra("google") != null)
        {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(Tokens.getDefault_web_client_id())
                    .requestEmail()
                    .build();

            GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            mGoogleSignInClient.revokeAccess();
            mAuth.signOut();
        }
        else
        {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            mAuth.signOut();
        }

        Intent intentVe = new Intent(this, IngresoActivity.class);
        intentVe.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentVe);
    }

    @Override
    public void onSaveInstanceState (Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putBundle(NOMBRE_BUNDLEAC, INFO_BUNDLEAC);
    }
}
