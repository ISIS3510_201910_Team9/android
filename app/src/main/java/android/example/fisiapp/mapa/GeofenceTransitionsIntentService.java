package android.example.fisiapp.mapa;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.example.fisiapp.R;
import android.example.fisiapp.login.IngresoActivity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;

public class GeofenceTransitionsIntentService extends IntentService {

    public static final String TAG = GeofenceTransitionsIntentService.class.getSimpleName();
    public static final int idNotificacion = 1111;
    public static final String canal = "canal_notificacion";

    public GeofenceTransitionsIntentService( ){
        super("GeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.d("entraAlNuevo","entraAlNuevo");

        if(!IngresoActivity.salioClinica)
        {
            return;
        }

        GeofencingEvent geoFencingEvent = GeofencingEvent.fromIntent(intent);

        if (geoFencingEvent.hasError()) {
            Log.e(TAG, String.format("Error code : %d", geoFencingEvent.getErrorCode()));
            return;
        }

        int getGeofenceTransition = geoFencingEvent.getGeofenceTransition();

        if(getGeofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER)
        {
            crearNotificacion(this, geoFencingEvent);
            IngresoActivity.salioClinica = false;
        }
        else if(getGeofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT)
        {
            crearNotificacion(this, geoFencingEvent);
            IngresoActivity.salioClinica = true;
        }
        else {
            // Log the error.
            Log.e(TAG, String.format("Unknown transition : %d", getGeofenceTransition));
            // No need to do anything else
            return;
        }
    }

    private void crearNotificacion(Context context, GeofencingEvent geoFencingEvent) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel mChannel = new NotificationChannel(canal, "Canal de mapa", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }

        String nombreSitio = "";
        for (LocacionPojo temp : Geofencing.locaciones) {

            Log.d("diferencialat", " " + (geoFencingEvent.getTriggeringLocation().getLatitude() - temp.getLatitud()));
            Log.d("diferencialon", " " + (geoFencingEvent.getTriggeringLocation().getLongitude() - temp.getLongitud()));

            if(Math.abs(geoFencingEvent.getTriggeringLocation().getLatitude() - temp.getLatitud()) < 0.0005 && Math.abs(geoFencingEvent.getTriggeringLocation().getLongitude() - temp.getLongitud()) < 0.0005)
            {
                nombreSitio = temp.getNombre();
                break;
            }
            else
                nombreSitio = "Clínica";
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, canal)
                .setSmallIcon(R.drawable.ic_sentadilla)
                .setLargeIcon(largeIcon(context))
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setContentTitle(nombreSitio)
                .setContentText("Estás cerca de una clínica")
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setContentIntent(contentIntent(context))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT < Build.VERSION_CODES.O )
        {
            builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        }
        notificationManager.notify(idNotificacion, builder.build());
    }

    private static PendingIntent contentIntent (Context context)
    {
        Intent startActivityIntent = new Intent(context, MapsActivityCurrentPlace.class);
        return PendingIntent.getActivity(context, idNotificacion, startActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static Bitmap largeIcon (Context context)
    {
        Resources res = context.getResources();
        Bitmap largeIcon = BitmapFactory.decodeResource(res, R.drawable.ic_sentadilla);
        return largeIcon;
    }
}