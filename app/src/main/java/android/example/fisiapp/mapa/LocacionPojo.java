package android.example.fisiapp.mapa;

import java.util.UUID;

public class LocacionPojo {

    private double latitud;
    private double longitud;
    private String nombre;
    private String mensaje;
    private String ID;

    public LocacionPojo(double platitud, double plongitud, String pnombre, String pmensaje)
    {
        latitud = platitud;
        longitud = plongitud;
        nombre = pnombre;
        mensaje = pmensaje;
        ID = UUID.randomUUID().toString();
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getID() {
        return ID;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
