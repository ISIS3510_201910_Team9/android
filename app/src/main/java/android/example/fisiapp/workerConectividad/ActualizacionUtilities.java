package android.example.fisiapp.workerConectividad;

import android.content.Context;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.support.annotation.NonNull;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.BackoffPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkManager;

public class ActualizacionUtilities {

    public static void scheduleChargingReminder(@NonNull final Context context, InfoEjercicioPojo infoPojo) {

        Data input = new Data.Builder()
                .putString("infoPojoId", infoPojo.getIdFireBase())
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        OneTimeWorkRequest request =
                new OneTimeWorkRequest.Builder(ActualizacionWorker.class)
                        .setInputData(input)
                        .setInitialDelay(30, TimeUnit.SECONDS)
                        .setBackoffCriteria(BackoffPolicy.LINEAR, 30000, TimeUnit.MILLISECONDS)
                        .setConstraints(constraints)
                        .build();

        WorkManager.getInstance()
                .enqueueUniqueWork(UUID.randomUUID().toString(), ExistingWorkPolicy.KEEP, request);
    }

}