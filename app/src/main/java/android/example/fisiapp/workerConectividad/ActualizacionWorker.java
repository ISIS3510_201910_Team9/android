package android.example.fisiapp.workerConectividad;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.work.Data;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class ActualizacionWorker extends Worker {

    public ActualizacionWorker(@NonNull Context appContext, @NonNull WorkerParameters params) {
        super(appContext, params);
    }

    @Override
    public ListenableWorker.Result doWork() {
        // Do your work here.
        Data input = getInputData();
        String infoPojoId = input.getString("infoPojoId");
        // Return a ListenableWorker.Result

        DocumentReference mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(infoPojoId);
        mDocRef.update("completitud", true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("correcto", "gj");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("correcto", "rip");
                    }
                });

        Data outputData = new Data.Builder()
                .putString("aaa", "aaa")
                .build();
        return Result.success(outputData);
    }

    @Override
    public void onStopped() {
        // Cleanup because you are being stopped.
    }
}