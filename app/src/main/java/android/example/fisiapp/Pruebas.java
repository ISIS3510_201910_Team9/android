package android.example.fisiapp;

import android.content.Intent;

import android.example.fisiapp.persistencia.EjercicioDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
//import android.view.Menu;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Pruebas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pruebas);

//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url ="http://api.waqi.info/feed/geo:4.7087315;-74.0306411/?token=b29a171aaeb8bd2cd4ea0bb6eeea755c331e184c";
//
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
//                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d("rep","Response: " + response.toString());
//                        try {
//                            Log.d("rep","Response: " + response.getJSONObject("data"));
//                            Log.d("rep","Response: " + response.getJSONObject("data").getString("aqi"));
//                            Log.d("rep","Response: " + response.getJSONObject("data").getJSONObject("city").getString("name"));
//                            Log.d("rep","Response: " + response.getJSONObject("data").getJSONObject("time").getString("s"));
//
//                        }catch (Exception e)
//                        {
//
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        // TODO: Handle error
//
//                    }
//                });
//
//        RequestQueue requestQueue;
//
//        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
//
//        Network network = new BasicNetwork(new HurlStack());
//
//        requestQueue = new RequestQueue(cache, network);
//
//        requestQueue.start();
//
//        requestQueue.add(jsonObjectRequest);
//

// Add the request to the RequestQueue.
//        final YouTubePlayerView mTubeView;
//        final YouTubePlayer.OnInitializedListener mInitializer;
//
//        mTubeView = (YouTubePlayerView) findViewById(R.id.video);
//
//        mInitializer = new YouTubePlayer.OnInitializedListener() {
//            @Override
//            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
////                List<String> videoList = new ArrayList<>();
////                videoList.add("sdf");
//                youTubePlayer.loadVideo("W4hTJybfU7s");
//            }
//
//            @Override
//            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//
//            }
//        };
//
//        mTubeView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mTubeView.initialize(Tokens.getApi_Key(), mInitializer);
//            }
//        });

//        AppExecutors.getInstance().diskIO().execute(new Runnable() {
//            @Override
//            public void run() {
//                mDb.ejercicioDao().nukeTable();
//            }
//        });

//        locaPojo.add(new LocacionPojo(4.7087315, -74.0306411, "prueba", "Haz click aquí si deseas contactarte con el sitio"));
//        locaPojo.add(new LocacionPojo(4.6028134, -74.0648835, "Uniandes", "Haz click aquí si deseas contactarte con el sitio"));
//        locaPojo.add(new LocacionPojo(4.7106726, -74.0313506, "Clínica El Bosque", "Haz click aquí si deseas contactarte con el sitio"));
//        locaPojo.add(new LocacionPojo(4.7065773, -74.0503513, "Clínica Reina Sofía", "Haz click aquí si deseas contactarte con el sitio"));
//        locaPojo.add(new LocacionPojo(4.6950612, -74.0333354, "Fundación Santa Fé", "Haz click aquí si deseas contactarte con el sitio"));

//        DocumentReference mDocRef;
//        Map<String, Object> mapita = new HashMap<>();
//        mapita.put("nombre", "prueba");
//        mapita.put("mensaje", "Haz click aquí si deseas contactarte con el sitio");
//        mapita.put("latitud", 4.7087315);
//        mapita.put("longitud", -74.0306411);
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionLocaciones").document("Bogota").collection("Sitios").document(UUID.randomUUID().toString());
//
//            mDocRef.set(mapita).addOnSuccessListener(new OnSuccessListener<Void>() {
//                @Override
//                public void onSuccess(Void aVoid) {
//                    Log.d("respuesta", "Documento Guardado");
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    Log.w("respuesta", "hubo un error", e);
//                }
//            });
//
//        Map<String, Object> mapita2 = new HashMap<>();
//        mapita2.put("nombre", "Uniandes");
//        mapita2.put("mensaje", "Haz click aquí si deseas contactarte con el sitio");
//        mapita2.put("latitud", 4.6028134);
//        mapita2.put("longitud", -74.0648835);
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionLocaciones").document("Bogota").collection("Sitios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita2).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita3 = new HashMap<>();
//        mapita3.put("nombre", "Clínica El Bosque");
//        mapita3.put("mensaje", "Haz click aquí si deseas contactarte con el sitio");
//        mapita3.put("latitud", 4.7106726);
//        mapita3.put("longitud", -74.0313506);
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionLocaciones").document("Bogota").collection("Sitios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita3).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita4 = new HashMap<>();
//        mapita4.put("nombre", "Clínica Reina Sofía");
//        mapita4.put("mensaje", "Haz click aquí si deseas contactarte con el sitio");
//        mapita4.put("latitud", 4.7065773);
//        mapita4.put("longitud", -74.0333354);
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionLocaciones").document("Bogota").collection("Sitios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita4).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita5 = new HashMap<>();
//        mapita5.put("nombre", "Fundación Santa Fé");
//        mapita5.put("mensaje", "Haz click aquí si deseas contactarte con el sitio");
//        mapita5.put("latitud", 4.6950612);
//        mapita5.put("longitud", -74.0503513);
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionLocaciones").document("Bogota").collection("Sitios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita5).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });

//        Map<String, Object> mapita5 = new HashMap<>();
//        mapita5.put("nombre", "Fundación Santa Fé");
//        mapita5.put("mensaje", "Haz click aquí si deseas contactarte con el sitio");
//        mapita5.put("latitud", 4.6950612);
//        mapita5.put("longitud", -74.0503513);
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionPreguntas").document("Bogota").collection("Sitios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita5).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita2 = new HashMap<>();
//        mapita2.put("nombre", "Sentadillas2");
//        mapita2.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita2.put("tipo", "Sentadillas2");
//        mapita2.put("repeticiones", 10);
//        mapita2.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita2.put("completitud", false);
//        mapita2.put("duracion", 10);
//        mapita2.put("cantidadRepeticiones", 5);
//        mapita2.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita2).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita3 = new HashMap<>();
//        mapita3.put("nombre", "Sentadillas3");
//        mapita3.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita3.put("tipo", "Sentadillas3");
//        mapita3.put("repeticiones", 10);
//        mapita3.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita3.put("completitud", false);
//        mapita3.put("duracion", 10);
//        mapita3.put("cantidadRepeticiones", 5);
//        mapita3.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita3).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita4 = new HashMap<>();
//        mapita4.put("nombre", "Sentadillas4");
//        mapita4.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita4.put("tipo", "Sentadillas4");
//        mapita4.put("repeticiones", 10);
//        mapita4.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita4.put("completitud", false);
//        mapita4.put("duracion", 10);
//        mapita4.put("cantidadRepeticiones", 5);
//        mapita4.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita4).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita5 = new HashMap<>();
//        mapita5.put("nombre", "Sentadillas5");
//        mapita5.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita5.put("tipo", "Sentadillas5");
//        mapita5.put("repeticiones", 10);
//        mapita5.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita5.put("completitud", false);
//        mapita5.put("duracion", 10);
//        mapita5.put("cantidadRepeticiones", 5);
//        mapita5.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita5).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//
//        Map<String, Object> mapita6 = new HashMap<>();
//        mapita6.put("nombre", "Sentadillas6");
//        mapita6.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita6.put("tipo", "Sentadillas6");
//        mapita6.put("repeticiones", 10);
//        mapita6.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita6.put("completitud", false);
//        mapita6.put("duracion", 10);
//        mapita6.put("cantidadRepeticiones", 5);
//        mapita6.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita6).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//
//        Map<String, Object> mapita7 = new HashMap<>();
//        mapita7.put("nombre", "Sentadillas7");
//        mapita7.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita7.put("tipo", "Sentadillas7");
//        mapita7.put("repeticiones", 10);
//        mapita7.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita7.put("completitud", false);
//        mapita7.put("duracion", 10);
//        mapita7.put("cantidadRepeticiones", 5);
//        mapita7.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita7).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//
//        Map<String, Object> mapita8 = new HashMap<>();
//        mapita8.put("nombre", "Sentadillas8");
//        mapita8.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita8.put("tipo", "Sentadillas8");
//        mapita8.put("repeticiones", 10);
//        mapita8.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita8.put("completitud", false);
//        mapita8.put("duracion", 10);
//        mapita8.put("cantidadRepeticiones", 5);
//        mapita8.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita8).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//
//        Map<String, Object> mapita9 = new HashMap<>();
//        mapita9.put("nombre", "Sentadillas9");
//        mapita9.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita9.put("tipo", "Sentadillas9");
//        mapita9.put("repeticiones", 10);
//        mapita9.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita9.put("completitud", false);
//        mapita9.put("duracion", 10);
//        mapita9.put("cantidadRepeticiones", 5);
//        mapita9.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita9).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita10 = new HashMap<>();
//        mapita10.put("nombre", "Sentadillas10");
//        mapita10.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita10.put("tipo", "Sentadillas10");
//        mapita10.put("repeticiones", 10);
//        mapita10.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita10.put("completitud", false);
//        mapita10.put("duracion", 10);
//        mapita10.put("cantidadRepeticiones", 5);
//        mapita10.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita10).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita11 = new HashMap<>();
//        mapita11.put("nombre", "Sentadillas11");
//        mapita11.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita11.put("tipo", "Sentadillas11");
//        mapita11.put("repeticiones", 10);
//        mapita11.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita11.put("completitud", false);
//        mapita11.put("duracion", 10);
//        mapita11.put("cantidadRepeticiones", 5);
//        mapita11.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita11).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita12 = new HashMap<>();
//        mapita12.put("nombre", "Sentadillas11");
//        mapita12.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita12.put("tipo", "Sentadillas11");
//        mapita12.put("repeticiones", 10);
//        mapita12.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita12.put("completitud", false);
//        mapita12.put("duracion", 10);
//        mapita12.put("cantidadRepeticiones", 5);
//        mapita12.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita12).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//
//        Map<String, Object> mapita13 = new HashMap<>();
//        mapita13.put("nombre", "Sentadillas11");
//        mapita13.put("descripcion", "Tienes que hacer 10 sentadillas");
//        mapita13.put("tipo", "Sentadillas11");
//        mapita13.put("repeticiones", 10);
//        mapita13.put("creacion", Calendar.getInstance().getTimeInMillis());
//        mapita13.put("completitud", false);
//        mapita13.put("duracion", 10);
//        mapita13.put("cantidadRepeticiones", 5);
//        mapita13.put("video", "https://www.youtube.com/watch?v=0Bcq3NXomaQ");
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(UUID.randomUUID().toString());
//
//        mDocRef.set(mapita13).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("respuesta", "Documento Guardado");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("respuesta", "hubo un error", e);
//            }
//        });
//        Log.d("correcto", "uno");
//
//        mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document("ejercicio1");
//        mDocRef.update("completitud", true)
//                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        Log.d("correcto", "gj");
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.d("correcto", "rip");
//                    }
//                });
//        Log.d("correcto", "dos");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_ejercicio, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()){
            case R.id.home:
                startActivity(new Intent(this, HomeActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
