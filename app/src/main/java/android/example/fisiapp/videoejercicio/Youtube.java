package android.example.fisiapp.videoejercicio;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.example.fisiapp.R;
import android.example.fisiapp.login.IngresoActivity;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.firebase.auth.FirebaseAuth;

import java.lang.ref.WeakReference;

public class Youtube extends AppCompatActivity {

    InfoEjercicioPojo objeto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);
        objeto = (InfoEjercicioPojo) getIntent().getExtras().get("objeto");
        Toolbar myToolbar = findViewById(R.id.my_toolbar_devolverse);
        myToolbar.setTitle("Video Explicación");
        setSupportActionBar(myToolbar);
        findViewById(R.id.contenedorVid).setVisibility(View.VISIBLE);
        reproducirVideo();
    }

    public void reproducirVideo() {

        WeakReference<YouTubePlayerFragment> youTubePlayerFragment = new WeakReference<>(YouTubePlayerFragment.newInstance());
        final YouTubePlayer.OnInitializedListener mInitializer;

        mInitializer = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(objeto.getVideo().split("v=")[1]);
                youTubePlayer.setShowFullscreenButton(false);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };

        youTubePlayerFragment.get().initialize(Tokens.getApi_Key(), mInitializer);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.contenedorVid, youTubePlayerFragment.get());
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.actionstoolbarmapa, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.sign_out:
                sign_out();
                return true;
            case R.id.devolverse:
                devolverse();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void devolverse()
    {
        this.finish();
    }

    private void sign_out()
    {
        if(getIntent().getStringExtra("google") != null)
        {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(Tokens.getDefault_web_client_id())
                    .requestEmail()
                    .build();

            GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            mGoogleSignInClient.revokeAccess();
            mAuth.signOut();
        }
        else
        {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            mAuth.signOut();
        }

        Intent intentVe = new Intent(this, IngresoActivity.class);
        intentVe.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentVe);
    }
}
