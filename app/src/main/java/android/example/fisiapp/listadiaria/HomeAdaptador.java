package android.example.fisiapp.listadiaria;

import android.content.Intent;
import android.example.fisiapp.R;
import android.example.fisiapp.verificacion.VerificacionPasosActivity;
import android.example.fisiapp.videoejercicio.Youtube;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.example.fisiapp.verificacion.VerificacionActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;

import java.util.ArrayList;

public class HomeAdaptador extends RecyclerView.Adapter<HomeAdaptador.NumberViewHolder>  {

    private static final String TAG = HomeAdaptador.class.getSimpleName();
    private final ListItemClickListener mOnClickListener;
    private int mNumberItems;
    public ArrayList<InfoEjercicioPojo> arreglo = new ArrayList<>();


    public interface ListItemClickListener{
        void onListItemClick(int clickedItemIndex, InfoEjercicioPojo infoEjercicioPojo);
    }

    public HomeAdaptador(int numberOfItems, ListItemClickListener listener) {
        mNumberItems = numberOfItems;
        mOnClickListener = listener;
    }

    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType){
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.recycler_ejercicios;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        NumberViewHolder viewHolder = new NumberViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        Log.d(TAG, "#" + position);
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    public void setArreglo(Bundle parreglo){
        for (String key : parreglo.keySet()) {
            InfoEjercicioPojo temp = (InfoEjercicioPojo) parreglo.get(key);
            arreglo.add(temp);
        }
        notifyDataSetChanged();
    }

    class NumberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView titulo;
        ImageView imagen;
        TextView tiempo;
        TextView cantidad;
        TextView repeticiones;
        ImageView check;
        Button botonValidar;
        Button botonVideo;
        View view;

        public NumberViewHolder(View itemView){
            super(itemView);

            titulo = (TextView) itemView.findViewById(R.id.tituloEjercicio);
            imagen = (ImageView) itemView.findViewById(R.id.imagenEjercicio);
            tiempo = (TextView) itemView.findViewById(R.id.tiempo);
            cantidad = (TextView) itemView.findViewById(R.id.cantidad);
            repeticiones = (TextView) itemView.findViewById(R.id.repeticiones);
            check = (ImageView) itemView.findViewById(R.id.check);
            botonValidar = itemView.findViewById(R.id.validar);
            botonVideo = itemView.findViewById(R.id.video);
            view = itemView;
            itemView.setOnClickListener(this);
        }

        void bind(int position)
        {
            try
            {
                final InfoEjercicioPojo temp = (InfoEjercicioPojo) arreglo.get(position);
                titulo.setText(temp.getNombre());
                if(temp.getTipo().equals("Caminata"))
                {
                    imagen.setImageResource(R.drawable.ic_man_walking);
                }
                else
                {
                    imagen.setImageResource(R.drawable.ic_muscle);
                }
                tiempo.setText(temp.getDuracion() + " min");
                cantidad.setText(temp.getRepeticiones() + " veces");
                repeticiones.setText(temp.getCantidadRepeticiones() + " repeticiones");

                if(temp.isCompletitud())
                {
                    check.setImageResource(R.drawable.ic_completado);
                }
                else {
                    check.setImageResource(R.drawable.ic_por_completar);
                }

                botonValidar.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        if (temp.getTipo().equals("Flexión")) {
                            Intent intentVe = new Intent(view.getContext(), VerificacionActivity.class);
                            intentVe.putExtra("objeto", temp);
                            view.getContext().startActivity(intentVe);
                        }else{
                            Intent intentVe = new Intent(view.getContext(), VerificacionPasosActivity.class);
                            intentVe.putExtra("objeto", temp);
                            view.getContext().startActivity(intentVe);
                        }
                    }
                });

                botonVideo.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        Intent intentVe = new Intent(view.getContext(), Youtube.class);
                        intentVe.putExtra("objeto",temp);
                        view.getContext().startActivity(intentVe);
                    }
                });

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition, arreglo.get(clickedPosition));
        }
    }
}