package android.example.fisiapp.verificacion;

import android.content.Context;
import android.content.DialogInterface;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.example.fisiapp.R;
import android.widget.Button;

import java.awt.font.NumericShaper;
import java.lang.ref.WeakReference;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Vector2Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class Vector2Fragment extends Fragment implements View.OnClickListener, SensorEventListener {

    private OnFragmentInteractionListener mListener;
    private Button activar_v2;
    private Button siguiente_v2;

    private SensorManager sensorManager;
    private Sensor rot_vector;

    private double xv1;
    private double yv1;
    private double zv1;

    private InfoEjercicioPojo objetoPojo;

    private static final double RANGO = 0.15;

    private double xv2;
    private double yv2;
    private double zv2;

    private boolean registrarVector;

    public Vector2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_vector2, container, false);
        // Inflate the layout for this fragment
        activar_v2=(Button) view.findViewById(R.id.button_define_vector2);
        siguiente_v2=(Button) view.findViewById(R.id.button_siguiente2);
        activar_v2.setOnClickListener(this);
        siguiente_v2.setOnClickListener(this);
        registrarVector=false;
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_define_vector2:
                registrarVector=true;
                break;
            case R.id.button_siguiente2:
                WeakReference<VerificacionFragment> vf = new WeakReference<>(new VerificacionFragment());
                vf.get().defineVectors(sensorManager,rot_vector,xv1,yv1,zv1,xv2,yv2,zv2,objetoPojo);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.contenedor_verif,vf.get(),"v3");
                ft.commit();
                break;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        double xa = event.values[0];
        double ya = event.values[1];
        double za = event.values[2];

        if (registrarVector){

            if (((xv1-RANGO<xa-RANGO && xa-RANGO<xv1+RANGO) || (xv1-RANGO<xa+RANGO && xa+RANGO<xv1+RANGO)) &&
                    ((yv1-RANGO<ya-RANGO && ya-RANGO<yv1+RANGO) || (yv1-RANGO<ya+RANGO && ya+RANGO<yv1+RANGO)) &&
                    ((zv1-RANGO<za-RANGO && za-RANGO<zv1+RANGO) || (zv1-RANGO<za+RANGO && za+RANGO<zv1+RANGO))) {

                AlertDialog.Builder builder= new AlertDialog.Builder(this.getContext(), R.style.MyDialogTheme);
                builder.setMessage(R.string.alert_dialog_v2);
                builder.setTitle(R.string.alert_dialog_v2_title);
                builder.setPositiveButton(R.string.message_vector_aceptar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activar_v2.setText(R.string.message_vector_aceptar);
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                registrarVector=false;
                siguiente_v2.setEnabled(false);

            }else{

                xv2=xa;
                yv2=ya;
                zv2=za;

                Log.d("SENSOR_XV1", String.valueOf(xv1));
                Log.d("SENSOR_YV1", String.valueOf(yv1));
                Log.d("SENSOR_ZV1", String.valueOf(zv1));
                Log.d("SENSOR_XV2", String.valueOf(xv2));
                Log.d("SENSOR_YV2", String.valueOf(yv2));
                Log.d("SENSOR_ZV2", String.valueOf(zv2));

                siguiente_v2.setEnabled(true);
                siguiente_v2.setTextColor(ContextCompat.getColor(getContext(), R.color.blancoIconos));
                activar_v2.setText(R.string.message_vector_actualizar);
                registrarVector=false;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void defineV1(SensorManager sm,Sensor s, double x, double y, double z, InfoEjercicioPojo obj){
        sensorManager=sm;
        rot_vector=s;
        xv1=x;
        yv1=y;
        zv1=z;
        objetoPojo=obj;
        sensorManager.registerListener(this, rot_vector,SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
