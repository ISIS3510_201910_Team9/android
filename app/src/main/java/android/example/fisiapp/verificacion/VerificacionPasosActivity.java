package android.example.fisiapp.verificacion;

import android.example.fisiapp.R;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.lang.ref.WeakReference;

public class VerificacionPasosActivity extends AppCompatActivity implements VerificacionPasos1Fragment.OnFragmentInteractionListener,VerificacionPasos2Fragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verificacion_pasos);

        WeakReference <VerificacionPasos1Fragment> vf = new WeakReference<>(new VerificacionPasos1Fragment());
        InfoEjercicioPojo temp = (InfoEjercicioPojo) getIntent().getExtras().get("objeto");
        vf.get().setObjectoPojo(temp);
        getSupportFragmentManager().beginTransaction().add(R.id.contenedor_verif_pasos,vf.get()).commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
