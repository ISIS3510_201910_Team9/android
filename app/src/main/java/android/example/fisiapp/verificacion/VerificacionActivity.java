package android.example.fisiapp.verificacion;

import android.content.Context;
import android.content.DialogInterface;
import android.example.fisiapp.R;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.hardware.SensorManager;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.j2objc.annotations.Weak;

import java.lang.ref.WeakReference;

public class VerificacionActivity extends AppCompatActivity implements Vector1Fragment.OnFragmentInteractionListener,
        Vector2Fragment.OnFragmentInteractionListener , VerificacionFragment.OnFragmentInteractionListener,
        VerifCompletaFragment.OnFragmentInteractionListener{


    private SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verificacion);

        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        WeakReference<Vector1Fragment> fv1 = new WeakReference<>(new Vector1Fragment());
        InfoEjercicioPojo temp = (InfoEjercicioPojo) getIntent().getExtras().get("objeto");
        fv1.get().setObjectoPojo(temp,sensorManager);
        getSupportFragmentManager().beginTransaction().add(R.id.contenedor_verif,fv1.get(),"v1").commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder= new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setMessage(R.string.alert_dialog_back_message);
        builder.setTitle(R.string.alert_dialog_back_title);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try{
                    sensorManager.unregisterListener((Vector1Fragment)getSupportFragmentManager().findFragmentByTag("v1"));
                }catch (Exception e){

                }
                try {
                    sensorManager.unregisterListener((Vector2Fragment)getSupportFragmentManager().findFragmentByTag("v2"));
                }catch (Exception e){

                }
                try{
                    sensorManager.unregisterListener((VerificacionFragment)getSupportFragmentManager().findFragmentByTag("v3"));
                }catch (Exception e){

                }
                finish();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
