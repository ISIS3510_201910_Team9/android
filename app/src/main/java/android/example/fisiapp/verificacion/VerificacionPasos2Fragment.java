package android.example.fisiapp.verificacion;

import android.content.Context;
import android.content.DialogInterface;
import android.example.fisiapp.persistencia.AppExecutors;
import android.example.fisiapp.persistencia.EjercicioDatabase;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.example.fisiapp.workerConectividad.ActualizacionUtilities;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.example.fisiapp.R;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VerificacionPasos2Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class VerificacionPasos2Fragment extends Fragment implements SensorEventListener, View.OnClickListener {

    private OnFragmentInteractionListener mListener;
    private TextView text;

    private ImageView imgSuccess;
    private TextView textPasos;
    private TextView textCaminata;
    private Button btncancelar;

    private SensorManager sensorManager;
    private Sensor sensorPasos;
    private Sensor acc;
    private ProgressBar pgbar;
    private InfoEjercicioPojo objetoPojo;
    private int pasosInic;
    private int pasosObj;
    private boolean inicio;
    private boolean termino;

    public VerificacionPasos2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view=inflater.inflate(R.layout.fragment_verificacion_pasos2, container, false);;
        text=(TextView)view.findViewById(R.id.textViewNumPasos);
        textPasos=(TextView)view.findViewById(R.id.textViewPasos);
        textCaminata=(TextView)view.findViewById(R.id.textViewLabelCaminata);
        imgSuccess=(ImageView)view.findViewById(R.id.imageViewSuccessCam);
        pgbar=(ProgressBar)view.findViewById(R.id.progressBarCaminata);
        btncancelar=(Button)view.findViewById(R.id.button_cancelar_pasos);
        btncancelar.setOnClickListener(this);
        inicio=true;
        termino=false;
        pgbar.setProgress(1);
        text.setText("0");
        sensorManager.registerListener(this, sensorPasos,SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this,acc,SensorManager.SENSOR_DELAY_FASTEST);
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(!termino) {
            if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
                if (inicio) {
                    pasosInic = (int) event.values[0];
                    inicio = false;
                    text.setText(String.valueOf((int) (event.values[0] - pasosInic)));
                } else {
                    text.setText(String.valueOf((int) (event.values[0] - pasosInic)));
                    pgbar.setProgress((int) (((event.values[0] - pasosInic) * 100) / pasosObj));
                    if((int) (event.values[0] - pasosInic)>=pasosObj){
                        verificarFirebase();
                        text.setVisibility(View.INVISIBLE);
                        textPasos.setVisibility(View.INVISIBLE);
                        imgSuccess.setVisibility(View.VISIBLE);
                        textCaminata.setText("Caminata completada");
                        btncancelar.setText("Terminar caminata");
                        unregister();
                        termino=true;
                    }
                }
            }
        }
    }

    public void definePasos(InfoEjercicioPojo temp, SensorManager sm, Sensor s) {
        pasosObj =temp.getRepeticiones().intValue();
        objetoPojo =temp;
        sensorManager=sm;
        sensorPasos=s;
        acc= sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_cancelar_pasos:
                if (!termino) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext(), R.style.MyDialogTheme);
                    builder.setMessage(R.string.alert_dialog_back_message_pasos);
                    builder.setTitle(R.string.alert_dialog_back_title_pasos);
                    builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            unregister();
                            getActivity().finish();
                        }
                    });
                    builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else{
                    getActivity().finish();
                }
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void unregister(){
        sensorManager.unregisterListener(this);
    }

    public void verificarFirebase(){

        final ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (!wifi.isConnectedOrConnecting() && !mobile.isConnectedOrConnecting()) {

            final EjercicioDatabase mDb = EjercicioDatabase.getInstance(getActivity().getApplicationContext());
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    objetoPojo.setCompletitud(true);
                    mDb.ejercicioDao().updateEjercicio(objetoPojo);
                }
            });

            ActualizacionUtilities.scheduleChargingReminder(getActivity(), objetoPojo);
        }
        else{
            DocumentReference mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(objetoPojo.getIdFireBase());
            mDocRef.update("completitud", true)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("correcto", "gj");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("correcto", "rip");
                        }
                    });

            final EjercicioDatabase mDb = EjercicioDatabase.getInstance(getActivity().getApplicationContext());
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    objetoPojo.setCompletitud(true);
                    mDb.ejercicioDao().updateEjercicio(objetoPojo);
                }
            });
        }
    }
    
}
