package android.example.fisiapp.verificacion;

import android.content.Context;
import android.example.fisiapp.historial.HistorialPojo;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.example.fisiapp.R;
import android.widget.Button;

import com.google.j2objc.annotations.Weak;

import java.lang.ref.WeakReference;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Vector1Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class Vector1Fragment extends Fragment implements View.OnClickListener, SensorEventListener {

    private OnFragmentInteractionListener mListener;
    private Button activar_v1;
    private Button siguiente_v1;

    private SensorManager sensorManager;
    private Sensor rot_vector;

    private double xv1;
    private double yv1;
    private double zv1;
    private InfoEjercicioPojo objetoPojo;

    private boolean registrarVector;


    public Vector1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_vector1, container, false);
        // Inflate the layout for this fragment
        activar_v1=(Button) view.findViewById(R.id.button_define_vector1);
        siguiente_v1=(Button) view.findViewById(R.id.button_siguiente1);
        activar_v1.setOnClickListener(this);
        siguiente_v1.setOnClickListener(this);
        registrarVector=false;
        rot_vector= sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        sensorManager.registerListener(this, rot_vector,SensorManager.SENSOR_DELAY_NORMAL);
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_define_vector1:
                registrarVector=true;
                activar_v1.setText(R.string.message_vector_actualizar);
                break;
            case R.id.button_siguiente1:
                WeakReference<Vector2Fragment> v2f = new WeakReference<>(new Vector2Fragment());
                v2f.get().defineV1(sensorManager,rot_vector, xv1,yv1,zv1,objetoPojo);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.contenedor_verif,v2f.get(),"v2");
                ft.commit();
                break;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        double xa = event.values[0];
        double ya = event.values[1];
        double za = event.values[2];


        if (registrarVector) {
            xv1 = xa;
            yv1 = ya;
            zv1 = za;
            Log.d("SENSOR_XV1", String.valueOf(xv1));
            Log.d("SENSOR_YV1", String.valueOf(yv1));
            Log.d("SENSOR_ZV1", String.valueOf(zv1));
            siguiente_v1.setEnabled(true);
            siguiente_v1.setTextColor(ContextCompat.getColor(getContext(), R.color.blancoIconos));
            registrarVector = false;
        }
    }

    public void setObjectoPojo(InfoEjercicioPojo x, SensorManager sm){
        objetoPojo = x;
        sensorManager=sm;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }
}
