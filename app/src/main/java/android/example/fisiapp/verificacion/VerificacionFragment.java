package android.example.fisiapp.verificacion;

import android.content.Context;
import android.content.DialogInterface;
import android.example.fisiapp.workerConectividad.ActualizacionUtilities;
import android.example.fisiapp.persistencia.AppExecutors;
import android.example.fisiapp.persistencia.EjercicioDatabase;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.example.fisiapp.R;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.lang.ref.WeakReference;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VerificacionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class VerificacionFragment extends Fragment  implements View.OnClickListener , SensorEventListener {

    private Button bttnCancelar;
    private TextView repeticiones;
    private TextView msg_user;
    private int rep;
    private InfoEjercicioPojo objetoPojo;
    private ProgressBar pgbar;

    private OnFragmentInteractionListener mListener;

    private SensorManager sensorManager;
    private Sensor rot_vector;
    private Sensor gyro;

    private double xv1;
    private double yv1;
    private double zv1;

    private static final double RANGO = 0.15;

    private double xv2;
    private double yv2;
    private double zv2;

    private boolean contar;
    private boolean contarRep;
    private boolean alertDialogCreado;
    private boolean velocidadMax;

    private MediaPlayer mp;

    public VerificacionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_verificacion, container, false);
        bttnCancelar = (Button)view.findViewById(R.id.button_cancelar_verif);
        bttnCancelar.setOnClickListener(this);
        repeticiones = (TextView)view.findViewById(R.id.textView_repeticiones);
        msg_user = (TextView)view.findViewById(R.id.textView_msg_verif);
        pgbar=(ProgressBar)view.findViewById(R.id.progressBarFlexion);
        pgbar.setProgress(1);
        rep=0;
        repeticiones.setText(String.valueOf(rep));
        contar=false;
        contarRep=false;
        alertDialogCreado=false;
        velocidadMax = false;
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_cancelar_verif:
                AlertDialog.Builder builder= new AlertDialog.Builder(this.getContext(), R.style.MyDialogTheme);
                builder.setMessage(R.string.alert_dialog_back_message);
                builder.setTitle(R.string.alert_dialog_back_title);
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        unregister();
                        getActivity().finish();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor s = event.sensor;

        if (s.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR){

            double xa = event.values[0];
            double ya = event.values[1];
            double za = event.values[2];

            try{

                if ((xv1-RANGO)<xa && xa<(xv1+RANGO) &&  (yv1-RANGO)<ya && ya<(yv1+RANGO) && (zv1-RANGO)<za && za<(zv1+RANGO)){

                    msg_user.setText(R.string.message_verif_2);

                    if (!contar){
                        contar=true;
                    }else{


                        if (contarRep && !velocidadMax) {
//                            verInternet();
                            rep++;
                            repeticiones.setText(String.valueOf(rep));
                            pgbar.setProgress((int)((rep*100)/objetoPojo.getRepeticiones()));
                            mp = MediaPlayer.create(this.getContext(), R.raw.beep);
                            mp.start();
                            contarRep = false;
                            if (rep == objetoPojo.getRepeticiones()) {
                                verificarFirebase();
                                sensorManager.unregisterListener(this, rot_vector);
                                sensorManager.unregisterListener(this, gyro);
                                WeakReference<VerifCompletaFragment> vcf = new WeakReference<>(new VerifCompletaFragment());
                                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                ft.replace(R.id.contenedor_verif, vcf.get());
                                ft.commit();
                            }
                        } else if (contarRep && velocidadMax) {
                            contarRep = false;
                            velocidadMax = false;
                        }

                    }
                }else if ((xv2-RANGO)<xa && xa<(xv2+RANGO) &&  (yv2-RANGO)<ya && ya<(yv2+RANGO) && (zv2-RANGO)<za && za<(zv2+RANGO) && contar){
                    msg_user.setText(R.string.message_verif_1);
                    contarRep=true;
                }
            }catch (Exception e){
                Log.d("ErrorVerif","ERROR---------------------------------");
            }
        } else if (s.getType() == Sensor.TYPE_GYROSCOPE){
            double xa = event.values[0];
            double ya = event.values[1];
            double za = event.values[2];
            double v = Math.sqrt(xa * xa + ya * ya + za * za);
            if (v>3.5){
                if (!alertDialogCreado) {
                    velocidadMax = true;
                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext(), R.style.MyDialogTheme);
                        builder.setMessage(R.string.gyroscope_fast_msg);
                        builder.setTitle(R.string.gyroscope_fast_title);
                        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialogCreado = false;
                                dialog.cancel();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        mp=MediaPlayer.create(this.getContext(),R.raw.error);
                        mp.start();
                        alertDialogCreado = true;
                    }catch (Exception e){

                    }
                }
            }
        }


    }

    public void verificarFirebase(){

        final ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (!wifi.isConnectedOrConnecting() && !mobile.isConnectedOrConnecting()) {

            final EjercicioDatabase mDb = EjercicioDatabase.getInstance(getActivity().getApplicationContext());
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    objetoPojo.setCompletitud(true);
                    mDb.ejercicioDao().updateEjercicio(objetoPojo);
                }
            });

            ActualizacionUtilities.scheduleChargingReminder(getActivity(), objetoPojo);
        }
        else{
            DocumentReference mDocRef = FirebaseFirestore.getInstance().collection("ColeccionEjercicios").document("Usuario1").collection("Ejercicios").document(objetoPojo.getIdFireBase());
            mDocRef.update("completitud", true)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("correcto", "gj");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("correcto", "rip");
                        }
                    });

            final EjercicioDatabase mDb = EjercicioDatabase.getInstance(getActivity().getApplicationContext());
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    objetoPojo.setCompletitud(true);
                    mDb.ejercicioDao().updateEjercicio(objetoPojo);
                }
            });
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void defineVectors(SensorManager sm, Sensor s, double x1, double y1, double z1, double x2, double y2, double z2, InfoEjercicioPojo obj){
        sensorManager=sm;
        rot_vector=s;
        xv1=x1;
        yv1=y1;
        zv1=z1;
        xv2=x2;
        yv2=y2;
        zv2=z2;
        objetoPojo=obj;
        gyro=sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorManager.registerListener(this, rot_vector,SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, gyro ,SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void unregister(){
        sensorManager.unregisterListener(this);
    }

}
