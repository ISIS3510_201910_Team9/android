package android.example.fisiapp.verificacion;

import android.content.Context;
import android.example.fisiapp.persistencia.InfoEjercicioPojo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.example.fisiapp.R;
import android.widget.Button;

import java.lang.ref.WeakReference;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VerificacionPasos1Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class VerificacionPasos1Fragment extends Fragment implements View.OnClickListener {

    private OnFragmentInteractionListener mListener;
    private Button botonIniciarCaminata;
    private SensorManager sensorManager;
    private Sensor sensorPasos;
    private InfoEjercicioPojo pojo;

    public VerificacionPasos1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view=inflater.inflate(R.layout.fragment_verificacion_pasos1, container, false);
        botonIniciarCaminata=(Button)view.findViewById(R.id.button_iniciar_caminata);
        botonIniciarCaminata.setOnClickListener(this);
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        sensorPasos=sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_iniciar_caminata:
                WeakReference<VerificacionPasos2Fragment> vf2 = new WeakReference<>(new VerificacionPasos2Fragment());
                vf2.get().definePasos(pojo,sensorManager,sensorPasos);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.contenedor_verif_pasos,vf2.get());
                ft.commit();
                break;
        }

    }

    public void setObjectoPojo(InfoEjercicioPojo temp) {
        pojo=temp;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
