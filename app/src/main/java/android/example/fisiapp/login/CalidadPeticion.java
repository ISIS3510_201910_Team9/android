package android.example.fisiapp.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.example.fisiapp.R;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

public class CalidadPeticion {

    public static FusedLocationProviderClient fusedLocationClient;
    public static Location mLastKnownLocation;
    public static boolean mLocationPermissionGranted;
    public static final String TAG = CalidadPeticion.class.getSimpleName();
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    public static void generarMensaje(final Activity activity, double[] numeros)
    {
        final String[] respuesta = new String[3];
        Log.d("aquiui10","wtf");

        if(numeros[0] == 0 && numeros[1] == 0)
        {
            return;
        }
        Log.d("aquiui11","wtf");

        double longitud = numeros[0];
        double latitud = numeros[1];
        respuesta[0] = "";
        respuesta[1] = "";
        respuesta[2] = "";

        String url ="http://api.waqi.info/feed/geo:"+latitud+";"+longitud+"/?token=b29a171aaeb8bd2cd4ea0bb6eeea755c331e184c";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("rep","Response: " + response.toString());
                        try {
                            Log.d("rep","Response: " + response.getJSONObject("data"));
                            Log.d("rep","Response: " + response.getJSONObject("data").getString("aqi"));
                            Log.d("rep","Response: " + response.getJSONObject("data").getJSONObject("city").getString("name"));
                            Log.d("rep","Response: " + response.getJSONObject("data").getJSONObject("time").getString("s"));
                            respuesta[0] = response.getJSONObject("data").getString("aqi");
                            respuesta[1] = response.getJSONObject("data").getJSONObject("city").getString("name");
                            respuesta[2] = response.getJSONObject("data").getJSONObject("time").getString("s");
                            Log.d("aquiui20","wtf" + respuesta[2]);

                            String titulo = "Calidad del aire";
                            String mensaje = "";

                            if(Integer.parseInt(respuesta[0]) > 150){
                                mensaje = "El día de hoy, la calidad del aire es de "+respuesta[0]+" en " + respuesta[1] + ". " + "Este valor es muy peligroso. Te recomendamos no salir afuera hoy.";
                            }
                            else if(Integer.parseInt(respuesta[0]) > 100)
                            {
                                mensaje = "El día de hoy, la calidad del aire es de "+respuesta[0]+" en " + respuesta[1] + ". " + "Este valor no es tan bueno pero tampoco grave. Evitar salir hoy sería la mejor opción.";
                            }
                            else {
                                mensaje = "El día de hoy, la calidad del aire es de "+respuesta[0]+" en " + respuesta[1] + ". " + "Este valor es bueno. Puedes salir a hacer estiramientos y ejercicios afuera.";
                            }

                            AlertDialog.Builder builder= new AlertDialog.Builder(activity, R.style.MyDialogTheme);
                            builder.setMessage(mensaje);
                            builder.setTitle(titulo);
                            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();

                        }catch (Exception e)
                        {

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });

        RequestQueue requestQueue;

        Cache cache = new DiskBasedCache(activity.getCacheDir(), 1024 * 1024); // 1MB cap

        Network network = new BasicNetwork(new HurlStack());

        requestQueue = new RequestQueue(cache, network);

        requestQueue.start();

        requestQueue.add(jsonObjectRequest);
    }

    public static void darCalidad(final Activity activity)
    {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        Log.d("aquiui","wtf");
        final double[] respuesta = new double[2];
        respuesta[0] = 0;
        respuesta[1] = 0;
        try {
            if (ContextCompat.checkSelfPermission(activity,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.d("aquiui2","wtf");

                Task<Location> locationResult = fusedLocationClient.getLastLocation();
                locationResult.addOnCompleteListener(activity, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            respuesta[0] = mLastKnownLocation.getLongitude();
                            respuesta[1] = mLastKnownLocation.getLatitude();
                            generarMensaje(activity, respuesta);

                        } else {
                            respuesta[0] = 0;
                            respuesta[1] = 0;
                            generarMensaje(activity, respuesta);
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                        }
                    }
                });
            }else
            {
                Log.d("aquiui4","" + respuesta[0]);

                ActivityCompat.requestPermissions(activity,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }
}
